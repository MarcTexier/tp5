# TP 5 - Une "vraie" topologie ?

### Setup clients

* PC 1(10.5.10.11) admin ping PC 3 admin egalement (10.5.10.12)

```
PC1> ping 10.5.10.12
84 bytes from 10.5.10.12 icmp_seq=1 ttl=64 time=0.645 ms
84 bytes from 10.5.10.12 icmp_seq=2 ttl=64 time=1.364 ms
84 bytes from 10.5.10.12 icmp_seq=3 ttl=64 time=0.913 ms
84 bytes from 10.5.10.12 icmp_seq=4 ttl=64 time=0.841 ms
84 bytes from 10.5.10.12 icmp_seq=5 ttl=64 time=1.152 ms

```

* PC 2 (10.5.20.11) guest ping PC 4 guest egalement (10.5.20.12)

```
PC2> ping 10.5.20.12
84 bytes from 10.5.20.12 icmp_seq=1 ttl=64 time=1.218 ms
84 bytes from 10.5.20.12 icmp_seq=2 ttl=64 time=1.289 ms
84 bytes from 10.5.20.12 icmp_seq=3 ttl=64 time=1.172 ms
84 bytes from 10.5.20.12 icmp_seq=4 ttl=64 time=0.804 ms
84 bytes from 10.5.20.12 icmp_seq=5 ttl=64 time=0.967 ms
```

### Setup VLANS

```
interface Ethernet0/0
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet0/1
 switchport access vlan 10
!
interface Ethernet0/2
 switchport access vlan 20
```

* PC 1(10.5.10.11) admin ping PC 3 admin egalement (10.5.10.12)

```
PC1> ping 10.5.10.12
84 bytes from 10.5.10.12 icmp_seq=1 ttl=64 time=0.645 ms
84 bytes from 10.5.10.12 icmp_seq=2 ttl=64 time=1.364 ms
84 bytes from 10.5.10.12 icmp_seq=3 ttl=64 time=0.913 ms
84 bytes from 10.5.10.12 icmp_seq=4 ttl=64 time=0.841 ms
84 bytes from 10.5.10.12 icmp_seq=5 ttl=64 time=1.152 ms
```


* PC 2 (10.5.20.11) guest ping PC 4 guest egalement (10.5.20.12)

```
PC2> ping 10.5.20.12
84 bytes from 10.5.20.12 icmp_seq=1 ttl=64 time=1.218 ms
84 bytes from 10.5.20.12 icmp_seq=2 ttl=64 time=1.289 ms
84 bytes from 10.5.20.12 icmp_seq=3 ttl=64 time=1.172 ms
84 bytes from 10.5.20.12 icmp_seq=4 ttl=64 time=0.804 ms
84 bytes from 10.5.20.12 icmp_seq=5 ttl=64 time=0.967 ms
```

* On change l'adresse ip du pc2 qui est dans le vlan guest on lui met une ip du réseau admin

```
PC2> ip 10.5.10.13 255.255.255.0
Checking for duplicate address...
PC1 : 10.5.10.13 255.255.255.0
```

* On va maintenant essayer de ping un pc admin
* voici la reponse

```
PC2> ping 10.5.10.11
host (10.5.10.11) not reachable
```

## II. Topologie 2 - VLAN, sous-interface, NAT

### 2. Adressage

* guest 3 ( 10.5.20.13) ping les 2 autres guest 

```
PC6> ping 10.5.20.11
84 bytes from 10.5.20.11 icmp_seq=1 ttl=64 time=1.400 ms
84 bytes from 10.5.20.11 icmp_seq=2 ttl=64 time=1.650 ms
84 bytes from 10.5.20.11 icmp_seq=3 ttl=64 time=1.111 ms
^C
PC6> ping 10.5.20.12
84 bytes from 10.5.20.12 icmp_seq=1 ttl=64 time=0.977 ms
84 bytes from 10.5.20.12 icmp_seq=2 ttl=64 time=0.955 ms
84 bytes from 10.5.20.12 icmp_seq=3 ttl=64 time=1.418 ms
```

* admin 3 ( 10.5.10.13) ping les 2 autres admins

```
PC5> ping 10.5.10.11
84 bytes from 10.5.10.11 icmp_seq=1 ttl=64 time=1.654 ms
84 bytes from 10.5.10.11 icmp_seq=2 ttl=64 time=1.293 ms
84 bytes from 10.5.10.11 icmp_seq=3 ttl=64 time=1.939 ms
^C
PC5> ping 10.5.10.12
84 bytes from 10.5.10.12 icmp_seq=1 ttl=64 time=1.143 ms
84 bytes from 10.5.10.12 icmp_seq=2 ttl=64 time=1.079 ms
84 bytes from 10.5.10.12 icmp_seq=3 ttl=64 time=0.970 ms
84 bytes from 10.5.10.12 icmp_seq=4 ttl=64 time=0.913 ms
```

### 3. VLAN

* Voici la conf du switch 1

```
interface Ethernet0/0
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet0/1
 switchport access vlan 10
!
interface Ethernet0/2
 switchport access vlan 20
```

* Voici la conf du switch 2

```
interface Ethernet0/0
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet0/1
 switchport access vlan 10
!
interface Ethernet0/2
 switchport access vlan 20
!
interface Ethernet0/3
!
interface Ethernet1/0
 switchport trunk encapsulation dot1q
 switchport mode trunk
```

* Voici la conf du switch 3

```
interface Ethernet0/0
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet0/1
 switchport access vlan 10
!
interface Ethernet0/2
 switchport access vlan 20
```

* On change l'adresse ip du pc6 qui est dans le vlan guest on lui met une ip du réseau admin

```
PC6> ip 10.5.10.14
Checking for duplicate address...
PC1 : 10.5.10.14 255.255.255.0
```

* On va maintenant essayer de ping un pc admin
* voici la reponse

```
PC6> ping 10.5.10.11
host (10.5.10.11) not reachable
```

### 4. Sous-interfaces

* Voici la conf de mon routeur

```
interface FastEthernet1/0
 no ip address
 duplex auto
 speed auto
!
interface FastEthernet1/0.10
 encapsulation dot1Q 10
 ip address 10.5.10.254 255.255.255.0
!
interface FastEthernet1/0.20
 encapsulation dot1Q 20
 ip address 10.5.20.254 255.255.255.0
!
```

* ping guest 3 vers gateway

```
PC6> ping 10.5.20.254
84 bytes from 10.5.20.254 icmp_seq=1 ttl=255 time=81.060 ms
84 bytes from 10.5.20.254 icmp_seq=2 ttl=255 time=12.913 ms
84 bytes from 10.5.20.254 icmp_seq=3 ttl=255 time=8.264 ms
84 bytes from 10.5.20.254 icmp_seq=4 ttl=255 time=12.795 ms
84 bytes from 10.5.20.254 icmp_seq=5 ttl=255 time=9.093 ms
```

* ping admin 1 vers gateway

```
PC1> ping 10.5.10.254
84 bytes from 10.5.10.254 icmp_seq=1 ttl=255 time=10.412 ms
84 bytes from 10.5.10.254 icmp_seq=2 ttl=255 time=10.017 ms
84 bytes from 10.5.10.254 icmp_seq=3 ttl=255 time=8.804 ms
84 bytes from 10.5.10.254 icmp_seq=4 ttl=255 time=10.070 ms
84 bytes from 10.5.10.254 icmp_seq=5 ttl=255 time=12.147 ms
```

### 4. NAT

On configure la carte NAT :
```bash
R1(config)#interface fastEthernet 0/0.10
R1(config-if)#ip nat inside
R1(config-if)#exit
R1(config)#interface fastEthernet 0/0.20
R1(config-if)#ip nat inside
R1(config)#interface fastEthernet 0/1
R1(config-if)#ip nat outside
R1(config-if)#exit
R1(config)#access-list 1 permit any
R1(config)#ip nat inside source list 1 interface fastEthernet 0/1 overload
```

Tentons de ping google.

```bash
PC1> ping 8.8.8.8  
*10.5.10.254 icmp_seq=1 ttl=255 time=54.009 ms (ICMP type:3, code:1, Destination host unreachable)  
*10.5.10.254 icmp_seq=2 ttl=255 time=3.532 ms (ICMP type:3, code:1, Destination host unreachable)  
*10.5.10.254 icmp_seq=3 ttl=255 time=12.028 ms (ICMP type:3, code:1, Destination host unreachable)
```
```bash
PC2> ping 8.8.8.8  
*10.5.20.254 icmp_seq=1 ttl=255 time=9.092 ms (ICMP type:3, code:1, Destination host unreachable)  
*10.5.20.254 icmp_seq=2 ttl=255 time=8.458 ms (ICMP type:3, code:1, Destination host unreachable)
```

## 3. Serveur DHCP

Maintenant on va mettre en place un service dhcp sur la VM, on installe le paquet dhcp avec la comamnde sudo yum install -y dhcp puis on écire les lignes qui suivante dans le fichier /etc/dhcp/dhcpd.conf

Voici la conf :

```
#
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp*/dhcpd.conf.example see dhcpd.conf(5) man page
#
# DHCP lease lifecycle
default-lease-time 600; max-lease-time 7200;
# This server is the only DHCP server we got So it is the authoritative one
authoritative;
# Configure logging
log-facility local7;
# Actually configure the DHCP server to serve our network
subnet 10.5.20.0 netmask 255.255.255.0 {
  # IPs that our DHCP server can give to client
  range 10.5.20.100 10.5.20.150;
  # Domain name served and DNS server (optional) The DHCP server gives this info to clients
  option domain-name "tp5.b1";
  option domain-name-servers 1.1.1.1;
  # Gateway of the network (optional) The DHCP server gives this info to clients
  option routers 10.5.20.254;
  # Specify broadcast addres of the network (optional)
  option broadcast-address 10.5.20.255;
}

```

On fait la commande `sudo systemctl start dhcp` pour demarrer le service.

On test le dhcp sur le guest 3 et il fonctionne : 

```
guest3> ip dhcp
DDORA IP 10.5.20.100/24 GW 10.5.20.254
```

Maintenant pour le serveur Web NGINX sur la VM web pour ce faire on installe le paquet epel-release avec la commande sudo yum install -y epel-release puis on installe le paquet nginx avec la commande sudo yum install -y nginx puis on ouvre le port 80 du pare-feu avec les commandes firewall-cmd --add-port=80/tcp --permanent puis on fait la commande firewall-cmd --reload puis on demarre le service nginx avec la commande sudo systemctl start gninx.

On cherche à vérifier que le serveur web est bien en fonction pour cela on fait un curl localhost:80 sur la VM web.

```
[Centos@web ~]$ curl localhost:80
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <title>Welcome to CentOS</title>
  <style rel="stylesheet" type="text/css">
        [...]
        /* Links */

        a:link { border-bottom: 1px dotted #ccc; text-decoration: none; color: #204d92; }
        a:hover { border-bottom:1px dotted #ccc; text-decoration: underline; color: green; }
        a:active {  border-bottom:1px dotted #ccc; text-decoration: underline; color: #204d92; }

        .logo a:link,
        .logo a:hover,
        .logo a:visited { border-bottom: none; }

        .mainlinks a:link { border-bottom: 1px dotted #ddd; text-decoration: none; color: #eee; }
        .mainlinks a:hover { border-bottom:1px dotted #ddd; text-decoration: underline; color: white; }
        .mainlinks a:active { border-bottom:1px dotted #ddd; text-decoration: underline; color: white; }
        .mainlinks a:visited { border-bottom:1px dotted #ddd; text-decoration: none; color: white; }
        .mainlinks a:visited:hover { border-bottom:1px dotted #ddd; text-decoration: underline; color: white; }
        [...]
  </style>
</head>
<body>
[...]
</body>
</html>
```

On test que le serveur web est bien fonction pour cela on fait un curl 10.5.30.12:80 sur la VM dns.


```
[antoine@dns ~]$ curl 10.5.30.12:80
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <title>Welcome to CentOS</title>
  <style rel="stylesheet" type="text/css">

        html {
        background-image:url(img/html-background.png);
        background-color: white;
        font-family: "DejaVu Sans", "Liberation Sans", sans-serif;
        font-size: 0.85em;
        line-height: 1.25em;
        margin: 0 4% 0 4%;
        }

        body {
        border: 10px solid #fff;
        margin:0;
        padding:0;
        background: #fff;
        [...]
  </style>

</head>

<body>
[...]
</body>
</html>
```

On veut mettre en place un serveur DNS sur la VM dns, on installe le paquet bind avec la commande sudo yum install -y bind puis on installe le paquet bind-utils avec la commande sudo yum install -y bind-util puis on écrit les lignes qui suivent dans le fichier /etc/named.conf avec la commande sudo nano /etc/named.conf

```
//
// named.conf
//
// Provided by Red Hat bind package to configure the ISC BIND named(8) DNS
// server as a caching only nameserver (as a localhost DNS resolver only).
//
// See /usr/share/doc/bind*/sample/ for example named configuration files.
//
// See the BIND Administrator's Reference Manual (ARM) for details about the
// configuration located in /usr/share/doc/bind-{version}/Bv9ARM.html

options {
    listen-on port 53 { 127.0.0.1; 10.5.30.11; };
    listen-on-v6 port 53 { ::1; };
    directory   "/var/named";
    dump-file   "/var/named/data/cache_dump.db";
    statistics-file "/var/named/data/named_stats.txt";
    memstatistics-file "/var/named/data/named_mem_stats.txt";
    recursing-file  "/var/named/data/named.recursing";
    secroots-file   "/var/named/data/named.secroots";
    allow-query     { 10.5.20.0/24; 10.5.30.0/24; };

    /* 
     - If you are building an AUTHORITATIVE DNS server, do NOT enable recursion.
     - If you are building a RECURSIVE (caching) DNS server, you need to enable 
       recursion. 
     - If your recursive DNS server has a public IP address, you MUST enable access 
       control to limit queries to your legitimate users. Failing to do so will
       cause your server to become part of large scale DNS amplification 
       attacks. Implementing BCP38 within your network would greatly
       reduce such attack surface 
    */
    recursion yes;

    dnssec-enable yes;
    dnssec-validation yes;

    /* Path to ISC DLV key */
    bindkeys-file "/etc/named.root.key";

    managed-keys-directory "/var/named/dynamic";

    pid-file "/run/named/named.pid";
    session-keyfile "/run/named/session.key";
};

logging {
        channel default_debug {
                file "data/named.run";
                severity dynamic;
        };
};

zone "." IN {
    type hint;
    file "named.ca";
};

zone "tp5.b1" IN {

         type master;

         file "/var/named/tp5.b1.db";

         allow-update { none; };
};

zone "20.5.10.in-addr.arpa" IN {

          type master;

          file "/var/named/20.5.10.db";

          allow-update { none; };
};

include "/etc/named.rfc1912.zones";
include "/etc/named.root.key";
```

Maintenant que l'on a écrit les lignes en dessus dans le fichier /etc/named.conf on va écrire les lignes qui suivent dans le fichier /var/named/tp5.b1.db

```
$TTL    604800
@   IN  SOA     ns1.tp5.b1. root.tp5.b1. (
                                                1001    ;Serial
                                                3H      ;Refresh
                                                15M     ;Retry
                                                1W      ;Expire
                                                1D      ;Minimum TTL
                                                )

;Name Server Information
@      IN  NS      ns1.tp5.b1.

ns1 IN  A       10.5.30.11
client1     IN  A       10.5.20.11
client2     IN  A       10.5.20.12
client3     IN  A       10.5.20.13

```

On va écrire dans le fichier /var/named/20.5.10.db

```
$TTL    604800
@   IN  SOA     ns1.tp5.b1. root.tp5.b1. (
                                                1001    ;Serial
                                                3H      ;Refresh
                                                15M     ;Retry
                                                1W      ;Expire
                                                1D      ;Minimum TTL
                                                )

;Name Server Information
@ IN  NS      ns1.tp5.b1.


11.30.5.10 IN PTR ns1.tp5.b1.

;PTR Record IP address to HostName
11      IN  PTR     client1.tp5.b1.
12      IN  PTR     client2.tp5.b1.
13      IN  PTR     client3.tp5.b1.
```

On démarre le service dns avec un systemctl start named puis on fait la commande sudo ss -ltunp pour voir qu'elle port est utilisé pour le service dns.

```
[Centos@dns ~]$ sudo ss -ltunp
Netid  State      Recv-Q Send-Q            Local Address:Port                           Peer Address:Port
udp    UNCONN     0      0                    10.5.30.11:53                                        *:*                   users:(("named",pid=2318,fd=513))
udp    UNCONN     0      0                     127.0.0.1:53                                        *:*                   users:(("named",pid=2318,fd=512))
udp    UNCONN     0      0                             *:68                                        *:*                   users:(("dhclient",pid=852,fd=6))
udp    UNCONN     0      0                         [::1]:53                                     [::]:*                   users:(("named",pid=2318,fd=514))
tcp    LISTEN     0      128                   127.0.0.1:953                                       *:*                   users:(("named",pid=2318,fd=24))
tcp    LISTEN     0      100                   127.0.0.1:25                                        *:*                   users:(("master",pid=1302,fd=13))
tcp    LISTEN     0      10                   10.5.30.11:53                                        *:*                   users:(("named",pid=2318,fd=22))
tcp    LISTEN     0      10                    127.0.0.1:53                                        *:*                   users:(("named",pid=2318,fd=21))
tcp    LISTEN     0      128                           *:22                                        *:*                   users:(("sshd",pid=1066,fd=3))
tcp    LISTEN     0      128                       [::1]:953                                    [::]:*                   users:(("named",pid=2318,fd=25))
tcp    LISTEN     0      100                       [::1]:25                                     [::]:*                   users:(("master",pid=1302,fd=14))
tcp    LISTEN     0      10                        [::1]:53                                     [::]:*                   users:(("named",pid=2318,fd=23))
tcp    LISTEN     0      128                        [::]:22                                     [::]:*                   users:(("sshd",pid=1066,fd=4))
```

Le résultat de la commande ss nous montre que les ports utilisent par le service dns sont les ports 53/udp et 953/tcp

Donc maintenant on va les ouvrir avec la commande sudo firewall-cmd --add-port=53/udp --permanent puis on fait la commande firewall-cmd --add-port=953/tcp --permanent puis on fait la commande firewall-cmd --reload pour vérifier que les ports sont bien ouverts on fait la commande sudo firewall-cmd --list-all

```
[Centos@dns ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: dhcpv6-client ssh
  ports: 53/udp 953/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

Le résultat de la commande nous montre que les ports 53/udp et 953/tcp sont bien ouverts.

Maintenant on veut vérifier que notre service dns est fonctionnel pour cela on vérifie qu'un gest peux ping google.com par exemple, on va utiliser comme serveur dns le service dns lancé sur la VM dns pour ce faire on fait la commande ip dns 10.5.30.11 sur le guest que l'on veut utiliser la c'est le guest3

```
guest3> ip dns 10.5.30.11
```

Maintenant on vérifie si le ping de google.com depuis guest3 marche.

```
guest3> ping google.com
google.com resolved to 216.58.204.142
84 bytes from 216.58.204.142 icmp_seq=1 ttl=53 time=226.752 ms
84 bytes from 216.58.204.142 icmp_seq=2 ttl=53 time=50.193 ms
84 bytes from 216.58.204.142 icmp_seq=3 ttl=53 time=51.126 ms
```

Le résultat du ping de google.com nous montre que le serveur dns qui est sur la VM dns est bien fonctionnel car il a transformer un nom de domaine en adresse IP.

On modifie la configuration du serveur dns pour que toutes les machines de l'infra puissent avoir la résolution de nom pour cela on va écrire les lignes suivantes dans le fichier /etc/named.conf

```
// 
// named.conf 
// 
// Provided by Red Hat bind package to configure the ISC BIND named(8) DNS 
// server as a caching only nameserver (as a localhost DNS resolver only). 
// 
// See /usr/share/doc/bind*/sample/ for example named configuration files. 
// 
// See the BIND Administrator's Reference Manual (ARM) for details about the 
// configuration located in /usr/share/doc/bind-{version}/Bv9ARM.html 

options {
    listen-on port 53 { 127.0.0.1; 10.5.30.11; };
    listen-on-v6 port 53 { ::1; };
    directory          "/var/named";
    dump-file          "/var/named/data/cache_dump.db";
    statistics-file    "/var/named/data/named_stats.txt";
    memstatistics-file "/var/named/data/named_mem_stats.txt";
    recursing-file     "/var/named/data/named.recursing";
    secroots-file      "/var/named/data/named.secroots";
    allow-query        { 10.5.10.0/24; 10.5.20.0/24; 10.5.30.0/24; };

    /*
     - If you are building an AUTHORITATIVE DNS server, do NOT enable recursion.
     - If you are building a RECURSIVE (caching) DNS server, you need to enable
       recursion.
     - If your recursive DNS server has a public IP address, you MUST enable access
       control to limit queries to your legitimate users. Failing to do so will
       cause your server to become part of large scale DNS amplification
       attacks. Implementing BCP38 within your network would greatly
       reduce such attack surface
    */
    recursion yes;

    dnssec-enable yes;
    dnssec-validation yes;

    /* Path to ISC DLV key */
    bindkeys-file "/etc/named.root.key";

    managed-keys-directory "/var/named/dynamic";

    pid-file "/run/named/named.pid";
    session-keyfile "/run/named/session.key";
};

logging {
        channel default_debug {
                file "data/named.run";
                severity dynamic;
        };
};

zone "." IN {
    type hint;
    file "named.ca";
};

zone "tp5.b1" IN {

         type master;

         file "/var/named/tp5.b1.db";

         allow-update { none; };
};

zone "20.5.10.in-addr.arpa" IN {

          type master;

          file "/var/named/20.5.10.db";

          allow-update { none; };
};

zone "10.5.10.in-addr.arpa" IN {

          type master;                                                                                                  
          file "/var/named/10.5.10.db";

          allow-update { none; };
};

include "/etc/named.rfc1912.zones"; 
include "/etc/named.root.key";
```

Après avoir modifier le fichier /etc/named.conf on fait la commande sudo systemctl reload named pour qu'il lis la modification dans le fichier de configuration

Maintenant on vérifie que nos modifications sont fonctionnelles pour cela on va dire à admin3 d'utiliser comme serveur dns le service dns lancé sur la VM dns pour ce faire on fait la commande ip dns 10.5.30.11 puis on va faire un ping de google.com à partir d'admin1

```
admin3> ip dns 10.5.30.11
```

Maintenant on vérifie si le ping de google.com depuis admin3 marche.

```
admin3> ping google.com
google.com resolved to 216.58.204.142
84 bytes from 216.58.204.142 icmp_seq=1 ttl=53 time=50.408 ms
84 bytes from 216.58.204.142 icmp_seq=2 ttl=53 time=43.985 ms
84 bytes from 216.58.204.142 icmp_seq=3 ttl=53 time=88.820 ms
```

Le résultat du ping de google.com nous montre que le serveur dns qui est sur la VM dns est bien fonctionnel car il a transformer un nom de domaine en adresse IP et qu'il est fonctionnel pour tous les machines de l'infra.

On veut ajouter deux options DHCP à notre service dncp, les deux options sont l'adresse de la passerelle et l'adresse du serveur DNS de l'infra pour cela on va écrire les lignes qui suivent dans le fichier /etc/dhcp/dhcpd.conf

```
#
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp*/dhcpd.conf.example see dhcpd.conf(5) man page
#
# DHCP lease lifecycle
default-lease-time 600; max-lease-time 7200;
# This server is the only DHCP server we got So it is the authoritative one
authoritative;
# Configure logging
log-facility local7;
# Actually configure the DHCP server to serve our network
subnet 10.5.20.0 netmask 255.255.255.0 {
  # IPs that our DHCP server can give to client
  range 10.5.20.100 10.5.20.150;
  # Domain name served and DNS server (optional) The DHCP server gives this info to clients
  option domain-name "tp5.b1";
  option domain-name-servers 10.5.30.11;
  # Gateway of the network (optional) The DHCP server gives this info to clients
  option routers 10.5.20.254;
  # Specify broadcast addres of the network (optional)
  option broadcast-address 10.5.20.255;
}
```

Après avoir modifier le fichier /etc/dhcp/dhcpd.conf on cherche à vérifier que nos modifications sont fonctionnelles pour cela on va donner une adresse IP en dhcp à guest3 en fessant la commande ip dhcp

```
guest3> ip dhcp
DDORA IP 10.5.20.103/24 GW 10.5.20.254
```

Le resultat de la commande nous montre que guest3 a bien récupérer une adresse IP en dhcp grâce à notre service dhcp qui tourne sur la VM dhcp maintenant on vérifie que guest3 à bien reçu l'adresse de la passerelle et l'adresse de la VM dns où il y a le service dns qui tourne pour cela on fait un ping de google.com qui nous prouve les deux en même temps.

```
guest3> ping google.com
google.com resolved to 216.58.204.142
84 bytes from 216.58.204.142 icmp_seq=1 ttl=53 time=71.881 ms
84 bytes from 216.58.204.142 icmp_seq=2 ttl=53 time=122.283 ms
84 bytes from 216.58.204.142 icmp_seq=3 ttl=53 time=56.412 ms
```

Le résultat du ping nous montre que guest3 à bien reçu l'adresse de la passerelle sinon il n'aurait pas pus aller sur Internet et qu'il a reçu l'adresse IP de la VM dns sinon il n'aurait pas pus convertir le nom de domaine en adresse IP.

